import 'package:week3/week3.dart' as week3;

void main(List<String> arguments) {
  double setengah, alas, tinggi;
  setengah = 0.5;
  alas = 20.0;
  tinggi = 30.0;

  print("Menghitung luas segitiga dengan alas = 20.0 dan tinggi 30.0");

  var luasSegitiga = setengah * alas * tinggi;

  print(luasSegitiga);
}
