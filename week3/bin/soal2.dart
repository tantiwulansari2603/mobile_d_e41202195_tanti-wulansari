import 'package:week3/week3.dart' as week3;

Future delayedPrint(int seconds, String pesan) {
  final duration = Duration(seconds: seconds);
  return Future.delayed(duration).then((value) => pesan);
}

void main(List<String> arguments) {
  print('Life ');
  delayedPrint(1, "never flat"). then((status){
    print(status);
  });
  print("is ");
}