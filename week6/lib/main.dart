import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

void main() {
  runApp(new MaterialApp(
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  final List<String> foto = [
    "chimmy.gif",
    "cooky.gif",
    "koya.gif",
    "mang.gif",
    "rj.gif",
    "shooky.gif",
    "tata.gif",
    "van.gif"
  ];

  static const Map<String, Color> colors = {
    "chimmy.gif": Colors.red,
    "cooky.gif": Colors.green,
    "koya.gif": Colors.blue,
    "mang.gif": Colors.yellow,
    "rj.gif": Colors.orange,
    "shooky.gif": Colors.purple,
    "tata.gif": Colors.pink,
    "van.gif": Colors.brown,
  };

  @override
  Widget build(BuildContext context) {
    timeDilation = 5.0;
    return new Scaffold(
      body: new Container(
        decoration: new BoxDecoration(
            gradient: new LinearGradient(
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
                colors: [
              Colors.white,
              Colors.purpleAccent,
              Colors.deepPurple
            ]
          )
        ),
        child: new PageView.builder(
          controller: new PageController(viewportFraction: 0.8),
          itemCount: foto.length,
          itemBuilder: (BuildContext context, int index) {
            return new Padding(
                padding:
                    new EdgeInsets.symmetric(horizontal: 5.0, vertical: 50.0),
                child: new Material(
                  elevation: 8.0,
                  child: new Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      new Hero(
                          tag: foto[index],
                          child: new Material(
                            child: new InkWell(
                              child: new Flexible(
                                flex: 1,
                                child: Container(
                                  color: colors.values.elementAt(index),
                                  child: new Image.asset(
                                    "img/${foto[index]}",
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              onTap: () => Navigator.of(context).push(
                                  new MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          new Halamandua(
                                            foto: foto[index],
                                            colors:
                                                colors.values.elementAt(index),
                                          ))),
                            ),
                          ))
                    ],
                  ),
                ));
          },
        ),
      ),
    );
  }
}

class Halamandua extends StatefulWidget {
  Halamandua({required this.foto, required this.colors});
  final String foto;
  final Color colors;

  @override
  _HalamanduaState createState() => _HalamanduaState();
}

class _HalamanduaState extends State<Halamandua> {
  Color warna = Colors.grey;

  void _pilihannya(Pilihan pilihan) {
    setState(() {
      warna = pilihan.warna;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Kucing Unyu"),
        backgroundColor: Colors.green,
        actions: <Widget>[
          new PopupMenuButton<Pilihan>(
            onSelected: _pilihannya,
            itemBuilder: (BuildContext context) {
              return listPilihan.map((Pilihan x) {
                return new PopupMenuItem<Pilihan>(
                  child: new Text(x.teks),
                  value: x,
                );
              }).toList();
            },
            )
        ],
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
              gradient: new RadialGradient(
                center: Alignment.center,
                colors: [Colors.white, Colors.yellow, Colors.green],
              ),
            ),
          ),
          new Center(
            child: new Hero(
                tag: widget.foto,
                child: new ClipOval(
                    child: new SizedBox(
                        width: 200.0,
                        height: 200.0,
                        child: new Material(
                          child: new InkWell(
                            onTap: () => Navigator.of(context).pop(),
                            child: new Flexible(
                              flex: 1,
                              child: Container(
                                color: widget.colors,
                                child: new Image.asset(
                                  "img/${widget.foto}",
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                        )))),
          )
        ],
      ),
    );
  }
}

class Pilihan{
  const Pilihan({required this.teks,required this.warna});
  final String teks;
  final Color warna;
}

List<Pilihan> listPilihan = const <Pilihan>[
  const Pilihan(teks: "Red", warna: Colors.red),
  const Pilihan(teks: "Green", warna: Colors.green),
  const Pilihan(teks: "Blue", warna: Colors.blue),
];

