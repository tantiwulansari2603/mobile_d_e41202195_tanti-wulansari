import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '/controller/demoController.dart';

class DemoPage extends StatelessWidget {
  final DemoController ctrl = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Demo Page'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(Get.arguments
              ),
            ),
            SwitchListTile(
              value: ctrl.isDark, 
              title: Text('Sentuh untuk mengubah tema'),
              onChanged: ctrl.changeTheme,
            ),
            Padding(padding: EdgeInsets.fromLTRB(8.0, 8.0, 10.0, 0.0),
            child: Column(
              children: <Widget>[
              ElevatedButton(
              onPressed: () => Get.snackbar(
                "Snackbar", "Hello this is the Snackbar message",
                snackPosition: SnackPosition.BOTTOM,
                colorText: Colors.white,
                backgroundColor: Colors.black87), 
              child: Text('Snack Bar')
            ),
            ],
            ),
            ),
            Padding(padding: EdgeInsets.fromLTRB(8.0, 8.0, 10.0, 0.0),
            child: Column(children: <Widget>[
              ElevatedButton(
              onPressed: () => Get.defaultDialog(
                title: "Dialogue",
                content: Text(
                  'Hey, From Dialogue',
                )), 
              child: Text('Dialogue')
            ),
            ],
            ),),
            
            Padding(padding: EdgeInsets.fromLTRB(8.0, 8.0, 10.0, 0.0),
            child: Column(children: <Widget>[
              ElevatedButton(
              onPressed: () => Get.bottomSheet(Container(
                height: 150,
                color: Colors.white,
                child: Text(
                  'Nama : Grup Jaya Makmur',
                  style: TextStyle(fontSize: 30.0),
                ),
              )), 
              child: Text('Data Diri')
            ),
            ],
            ),),
            
            Padding(padding: EdgeInsets.fromLTRB(8.0, 8.0, 10.0, 0.0),
            child: Column(children: <Widget>[
              ElevatedButton(
              onPressed: () => Get.offNamed('/'), 
              child: Text('Dashboard')
            ),
            ],),),
            
          ],
        ),
      ),
    );
  }
}