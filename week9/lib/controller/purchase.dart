import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import '/model/product.dart';

class Purchase extends GetxController {
  var products = <Product>[].obs;

  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    await Future.delayed(Duration(seconds: 1));

    var serverResponse = [
      Product(1, "PT.Alfaria Trijaya Tbk", "assets/img/alfa.jpg", 
        "IT Network Security", 300.0),
      Product(1, "PT Hebros", "assets/img/hebros.jpg", 
        "Sebior Staff IT", 300.0),
      Product(1, "PT JAC Indonesia", "assets/img/jac.png", 
        "IT Assistant Manager", 300.0),
      Product(1, "PT Global Jet Express", "assets/img/jnt.jpg", 
        "Operation Engineer", 300.0),
      Product(1, "PT Mane Indonesia", "assets/img/mane.jpg", 
        "Network Engineer", 300.0),
      Product(1, "PT Pulau Instant", "assets/img/pulau.png", 
        "Network System Administrator", 300.0),
    ];

    products.value = serverResponse;
  }
}